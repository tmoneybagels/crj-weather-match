import React from 'react';
import PropTypes from 'prop-types';

const Searchbar = () => {
  return (
    <form>
      <input type="text" placeholder="Search..." />
    </form>
  );
}

export default Searchbar;